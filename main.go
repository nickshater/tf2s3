package main

import (
	"fmt"
	"os"

	"gitlab.com/nickshater/tf2s3/fs"
	"gitlab.com/nickshater/tf2s3/s3"
)

func main() {
	if len(os.Args) <= 1 {
		fmt.Println("Please provide path as argument")
		os.Exit(1)
	}

	path := os.Args[1]
	files := fs.FindState(path)

	s := s3.MakeSession()

	for _, file := range files {
		fmt.Println(file)
		err := s3.UploadToS3(s, file)
		if err != nil {
			fmt.Println("error: ", err)
		}
	}
}
