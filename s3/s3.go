package s3

import (
	"bytes"
	"fmt"
	"net/http"
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

//MakeSession creates an aws session
func MakeSession() *session.Session {

	s3Region := os.Getenv("TFS_S3_REGION")
	s, err := session.NewSession(&aws.Config{Region: aws.String(s3Region)})
	if err != nil {
		fmt.Println("error with session: ", err)
	}

	return s
}

//UploadToS3 uploads a file to the s3 bucket
func UploadToS3(s *session.Session, file string) error {
	s3file, err := os.Open(file)
	if err != nil {
		fmt.Println("error with upload: ", err)
	}

	defer s3file.Close()

	fileInfo, _ := s3file.Stat()
	size := fileInfo.Size()
	buffer := make([]byte, size)
	s3file.Read(buffer)

	_, err = s3.New(s).PutObject(&s3.PutObjectInput{
		Bucket:               aws.String(os.Getenv("TFS_S3_BUCKET")),
		Key:                  aws.String(file),
		ACL:                  aws.String("private"),
		Body:                 bytes.NewReader(buffer),
		ContentLength:        aws.Int64(size),
		ContentType:          aws.String(http.DetectContentType(buffer)),
		ContentDisposition:   aws.String("attachment"),
		ServerSideEncryption: aws.String("AES256"),
	})
	return err
}
