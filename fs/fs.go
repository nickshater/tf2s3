package fs

import (
	"fmt"
	"os"
	"path/filepath"
)

//FindState will return a slice of tfstate files
func FindState(rootpath string) []string {
	var files []string

	err := filepath.Walk(rootpath, func(path string, info os.FileInfo, err error) error {
		if info.IsDir() {
			return nil
		}
		if filepath.Ext(path) == ".tfstate" {
			files = append(files, path)
		}
		return nil
	})
	if err != nil {
		fmt.Println("Error with walk: ", err)
	}
	return files
}
