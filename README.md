# tf2s3

tf2s3 sends all .tfstate files within a path to s3

# Setup

Set the following environment variables
### TFS_S3_REGION
The region the bucket is in

Ex: us-east-1
### TFS_S3_BUCKET
The bucket name

Ex: samplebucketname

# Usage

```
$ go build
$ ./tf2s3 path-to-crawl-for-state
```
